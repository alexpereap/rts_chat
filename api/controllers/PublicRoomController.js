/**
 * PublicRoomControllerController
 *
 * @description :: Server-side logic for managing Publicroomcontrollers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var mPublicRoomName = 'publicRoom';
var moment = require('moment');
moment.locale('es');

module.exports = {
    
    'index': function(req,res,next){
        
        res.view('chat/public_room/index');
    },
    
    'subscribe': function(req,res,next){
        
        // subscribe to public room
        try {
            
            var reqData = req.params.all();
            console.log(reqData);

            var nickname = reqData.nickname;
            nickname = nickname.trim();
            // TODO: nickname taken verification
            
            // sails.sockets.join(req.socket, mPublicRoomName);
            req.socket.nickname = nickname;
            sails.sockets.join(req.socket, mPublicRoomName, function(err){
                if (err) {
                    return res.serverError(err);
                }
                
                var socket_id = sails.sockets.getId(req);
                
                req.session.authenticated = true;
                req.session.thisUser = {
                    nickname: nickname,
                    socket_id: socket_id
                };
                
                sails.io.sockets.in(mPublicRoomName).clients(function(err,socketIds){
                    console.log(socketIds);
                });
                
                transmitionData = {
                    nickname: nickname,
                    socket_id: socket_id
                };
                
                sails.sockets.broadcast(mPublicRoomName, 'newUser', transmitionData);

                res.json({
                    message: 'Subscribed to room : "' + mPublicRoomName + '")',
                    success: true
                });
            });
            
        } catch(err){
            
            console.log(err);
            res.json({
                success: false,
                message: err
            });
        }
        
    },
    'sendMessage': function(req,res,next){
        transmitionData = {
            nickname: req.session.thisUser.nickname,
            socket_id: req.session.thisUser.socket_id,
            message: HelperService.urlify(req.param('message')),
            date : moment( new Date() ).format('MMMM - YYYY, h:mm:ss a')
        };
        
        console.log(transmitionData);

        sails.sockets.broadcast(mPublicRoomName, 'newMessage', transmitionData);
    },
    'userTyping':function(req,res,next){
        transmitionData = {
            nickname: req.session.thisUser.nickname,
            socket_id: req.session.thisUser.socket_id
        };
        sails.sockets.broadcast(mPublicRoomName, 'userIsTyping', transmitionData,req);
    }
    
};

