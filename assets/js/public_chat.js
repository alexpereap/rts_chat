// Angular APP
var chatApp = angular.module('chatApp',["ngSanitize"]);
var csrf;

chatApp.config(function($provide){
    $provide.decorator("$sanitize", function($delegate, $log){
        return function(text, target){
 
            var result = $delegate(text, target);
            $log.info("$sanitize input: " + text);
            $log.info("$sanitize output: " + result);
 
            return result;
        };
    });
});

window.onload = function init(){
    
    console.log("app READY");
    io.socket.get('/csrfToken', function (resData) {
        csrf = resData._csrf;
    });
    
} // load end

chatApp.controller('publicRoom',function($scope){
    console.log("Public room controller loaded");
    
    $scope.newUserVisible = false;
    $scope.publicRoomMessages = [];
    $scope.userMessage = '';
    
    // shows nickname insert modal
    $('#welcomeModal').modal({
        show: true
    });
    
    // new User
    io.socket.on('newUser', function onServerSentEvent (msg) {
        console.log("New user joined the chat room");
        console.log(msg);
        
        $scope.newUserVisible = true;
        $scope.newUser = msg.nickname;
        $scope.$digest();
        
    });
    
    // new message in the chat
    io.socket.on('newMessage', function onServerSentEvent (msg) {
       console.log(msg);
       $scope.publicRoomMessages.push({
           nickname: msg.nickname,
           body: msg.message,
           date: msg.date
       });
       
       $scope.$digest();
       $("#messagesWindow").animate({ scrollTop: $('#messagesWindow').prop("scrollHeight")}, 1000);
    });
    
    // user sends message 
    $scope.submitMessage = function () {
        
        // TODO: validation for empty message
        
        sendData = {
            message: $scope.userMessage.trim(),
            _csrf: csrf
        };

        $scope.userMessage = null;

        // socket request to subscribe public room
        io.socket.post('/public-room/send-message', sendData, function (resData) {
            console.log(resData);
        });
    }
    
    // user is writing in client
    $("#userMessage").keydown(function(event){
       
        if (event.which != 13) {
            sendData = { _csrf: csrf };

            io.socket.post('/public-room/user-typing', sendData, function (resData) {
                console.log(resData);
            });
        } 
        
    });
    
    // new user is writing in server
    var messageTimeOut = setTimeout(function () {
        $scope.userIsTyping = false;
        $scope.$digest();
    }, 500);
    io.socket.on('userIsTyping', function onServerSentEvent (msg) {
        
        $scope.userTypingNickname = msg.nickname;
        
        $scope.userIsTyping = true;
        window.clearTimeout(messageTimeOut);
        messageTimeOut = setTimeout(function(){
            $scope.userIsTyping = false;
            $scope.$digest();
        },500);        
        
        $scope.$digest();
    });
});

chatApp.controller('welcomeModalController', function ($scope) {
    
    $scope.formDisabled = false;

    $scope.subscribeToRoom = function () {
        
        // disables form controls
        $scope.formDisabled = true;

        sendData = {
            nickname: $scope.welcomeNickName,
            _csrf: csrf
        };
        
        // socket request to subscribe public room
        io.socket.post('/public-room/subscribe', sendData, function (resData) {
            console.log(resData);
            
            if(resData.success === true){
                $('#welcomeModal').modal('hide');
            }
            
        });
    }

});